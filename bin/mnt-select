#!/usr/bin/env bash

set -e

# Inspired by https://gist.github.com/oneohthree/f528c7ae1e701ad990e6
slug(){ echo "$@" | tr '[:upper:]' '[:lower:]' | tr ' ' '-'; }

case "$1" in
  '')
    OUTPUT='TYPE'
    SEDARG=''
    ;;
  'u' | 'up')
    # Note: the sed checks for 'part  ', which is the case when no MOUNTPOINT is given
    OUTPUT='TYPE,MOUNTPOINT'
    SEDARG='/^ /!d; s/^ //'
    ;;
  'd' | 'down')
    # Note: the sed checks for 'part  ', which is the case when no MOUNTPOINT is given
    OUTPUT='TYPE,MOUNTPOINT'
    SEDARG='/^\/mnt/!d; s/^[a-z/_-]+ //'
    ;;
  *)
    echo "Argument needs to be either empty, 'up', or 'down'" 2>&1
    exit 1
    ;;
esac

if [ -z "$FILTER" ]; then
  FILTER="/(recovery|reserved)/d"
fi

SELECTION="$(lsblk --raw --output "$OUTPUT,NAME,PARTTYPENAME" | sed -E "/^part /!d; s/^part //; $SEDARG; s/ /: /; s/\\\x20/ /g; $FILTER" | fzf --preview='mnt-preview {}')"

if [ -z "$SELECTION" ]; then
  exit 0
fi

IFS=": " read -r DEVICE TARGET <<< "$SELECTION"

echo "/dev/$DEVICE /mnt/$(slug "$TARGET")"
