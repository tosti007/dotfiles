#!/usr/bin/env bash
# modified by me, orignal source from: https://gist.github.com/davejamesmiller/1965569 http://djm.me/ask
## Asks the user the given prompt. Returns 0 when on 'yes', 1 on 'no', and exits with 0 on 'quit'.
##
## $1: prompt to ask
## $2: default answer, either 'Y' or 'N' or empty

ask() {
  local PROMPT DEFAULT REPLY

  while true; do
    if [ "${2:-}" = 'Y' ]; then
      PROMPT='Y/n'
      DEFAULT=Y
    elif [ "${2:-}" = 'N' ]; then
      PROMPT='y/N'
      DEFAULT=N
    else
      PROMPT='y/n'
      DEFAULT=
    fi

    # Ask the question (not using "read -p" as it uses stderr not stdout)
    echo -n "$1 [$PROMPT] "

    # Read the answer (use /dev/tty in case stdin is redirected from somewhere
    # else)
    read -r REPLY </dev/tty

    # Default?
    if [ -z "$REPLY" ]; then
      REPLY=$DEFAULT
    fi

    # Check if the reply is valid
    case $REPLY in
    Y* | y*) return 0 ;;
    N* | n*) return 1 ;;
    q*) exit 0 ;;
    esac
  done
}

# https://stackoverflow.com/questions/2683279/how-to-detect-if-a-script-is-being-sourced
if ! (return 0 2>/dev/null); then
  ask "$@"
fi

