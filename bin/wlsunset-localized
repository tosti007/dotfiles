#!/usr/bin/env bash
#
# wlsunset, with automatic argument setting based on the current timedatectl timezone.

TIMEZONE_FILE="$(dirname "$0")/../assets/timezone-coordinates.csv"
CONFIG_FILE="${XDG_CONFIG_HOME:-"$HOME/.config"}/wlsunset/config"

# Default values, outside of wlsunset's own arguments
temp_low=''
temp_high=''
sunrise=''
sunset=''
duration=''
gamma=''

# Overwriting values
if [ -f "$CONFIG_FILE" ]; then
	source "$CONFIG_FILE"
fi

# Finding location and adding to args
TIMEZONE="$(timedatectl | sed '/Time zone:/!d; s|.*: ||; s| .*||; s|/|-|')"
COORDINATES="$(sed "s|/|-|; /$TIMEZONE/!d" "$TIMEZONE_FILE")"
GEO_LATITUDE="$(echo "$COORDINATES" | cut -d ',' -f 3)"
GEO_LONGITUDE="$(echo "$COORDINATES" | cut -d ',' -f 4)"

echo $TIMEZONE
echo "$COORDINATES"
echo $GEO_LATITUDE
echo $GEO_LONGITUDE
LATITUDE="${GEO_LATITUDE:-"$LATITUDE"}"
LONGITUDE="${GEO_LONGITUDE:-"$LONGITUDE"}"

echo 'Obtained location'
echo " - Timezone: $TIMEZONE"
echo " - Latitude: $LATITUDE"
echo " - Longitude: $LONGITUDE"

# W/S = -
if [[ "$LATITUDE" == *S ]]; then
	LATITUDE="-$LATITUDE"
fi
if [[ "$LONGITUDE" == *W ]]; then
	LONGITUDE="-$LONGITUDE"
fi

SUNSETARGS="-l ${LATITUDE::-1} -L ${LONGITUDE::-1}"

if [ -n "$temp_low" ]; then
	SUNSETARGS="$SUNSETARGS -t $temp_low"
fi

if [ -n "$temp_high" ]; then
	SUNSETARGS="$SUNSETARGS -T $temp_high"
fi

if [ -n "$sunrise" ]; then
	SUNSETARGS="$SUNSETARGS -S $sunrise"
fi

if [ -n "$sunset" ]; then
	SUNSETARGS="$SUNSETARGS -s $sunset"
fi

if [ -n "$duration" ]; then
	SUNSETARGS="$SUNSETARGS -d $duration"
fi

if [ -n "$gamma" ]; then
	SUNSETARGS="$SUNSETARGS -g $gamma"
fi

# Starting wlsunset
wlsunset $SUNSETARGS $@
