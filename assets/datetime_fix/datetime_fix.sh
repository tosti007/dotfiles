#!/usr/bin/sh

###############################################################
# datetime fix for dual boot
###############################################################
echo "BEFORE RUNNING THIS:"
echo "Please copy datetime_fix.reg to windows and apply it to the registry"
sleep 2

pacman -S ntp
systemctl start ntpd.service
sudo timedatectl set-ntp true
sudo timedatectl set-timezone Europe/Amsterdam
pacman -Rsn ntp
