#!/usr/bin/env python
# Generate the background file

from string import Template
from json import loads as load_json
from os import getenv, chdir, path, makedirs
from sys import argv
from random import seed as set_seed
from random import randrange as rand
from colour import Color
import subprocess

# Goto the correct directory
chdir(path.dirname(path.realpath(argv[0])))

from settings import *

def read_file(file):
    text_file = open(file, 'r')
    data = text_file.read()
    text_file.close()
    return data

def read_template(file):
    return Template(read_file(file))

def read_colors():
    return load_json(read_file(getenv('HOME')+'/.cache/ruwal/theme.json'))

def read_color_value(ids, colors):
    ids = ids.split('+')
    c = [0, 0, 0]
    for i in ids:
        c = [a + b for a, b in zip(c, Color(colors[i]).rgb)]
    return Color(rgb=(x / len(ids) for x in c)).hex

def read_color_order(idx, order, colors):
    return {
        'nr': idx,
        'frame': read_color_value(order[0], colors),
        'slide': read_color_value(order[1], colors),
        'indent': read_color_value(order[2], colors),
        'bar': read_color_value(order[3], colors)
    }

def create_colors(orders, colors):
    return [read_color_order(n, o, colors) for n, o in enumerate(orders)]

# Set the seed
if seed != None:
    set_seed(seed)

main['size_x'] = 110 + main['margin'] * 2
main['size_y'] = 110 + main['margin'] * 2

colors_data = read_colors()
colors_orders = create_colors(colors, colors_data['color'])

colors_template = read_template('template_colors.txt')
colors_output = '\n'.join(colors_template.substitute(**cs) for cs in colors_orders)

items_data = []
for x in range(main['nx']):
    for y in range(main['ny']):
        items_data.append({
            'colors':  rand(0, len(colors_orders)),
            'x': main['size_x'] * x + main['margin'],
            'y': main['size_y'] * y + main['margin']
        })

item_template = read_template('template_item.txt')
item_output = '\n'.join(item_template.substitute(**it) for it in items_data)
item_width = main['size_x'] * main['scale'] * main['nx'] / 2
item_height = main['size_y'] * main['scale'] * main['ny'] / 2

main_template = read_template('template_main.txt')
main_output = main_template.substitute(
    background=colors_data['special']['background'],
    colors=colors_output,
    items=item_output,
    width=main['width'],
    height=main['height'],
    translate=(main['transform_x'] + main['width'] / 2 - item_width, main['transform_y'] + main['height'] / 2 - item_height),
    rotate=(main['angle'], item_width, item_height),
    scale=main['scale'],
    shadow_color=read_color_value(shadow['color'], colors_data['color']),
    shadow_dx=shadow['dx'],
    shadow_dy=shadow['dy'],
    shadow_std_deviation=shadow['std_deviation'],
    shadow_opacity=shadow['opacity']
)


outpath = getenv('HOME')+'/.cache/background/'
makedirs(outpath, exist_ok=True)
f = open(outpath+'main.svg', 'w')
f.write(main_output)
f.close()

# create png image
subprocess.run(['rsvg-convert', outpath+'main.svg', '--output', outpath+'main.png'])

for t in filetypes or []:
    subprocess.run(['convert', outpath+'main.png', outpath+f'main.{t}'])

