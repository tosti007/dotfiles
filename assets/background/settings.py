#!/usr/bin/env python
# Settings for background generation

# A seed to ensure that we get the same image every time
seed = 25565

# The colors ids as defined by pywal.
# Each tuple represents a combination of colors.
# A random tuple will be picked for each floppy.
# Using x0+x1+...+xn will result in the average of the listed colors.
colors = [
    ('blue', 'cyan+green', 'yellow', 'red'),
    ('cyan+green', 'red', 'blue', 'yellow'),
    ('yellow', 'blue', 'red', 'cyan+green'),
]

# Location and size settings
# Each setting is explained by the comment after it.
# All floppys will be placed on the center of the screen.
main = {
    'width': 1920, # Output image width
    'height': 1080, # Output image height
    'angle': 45, # Rotation angle of the floppys
    'transform_x': 0, # Transform x distance
    'transform_y': 0, # Transform y distance
    'nx': 8, # Number of floppys in x direction
    'ny': 8, # Number of floppys in the y direction
    'scale': 1.5, # Scale to resize each floppy with
    'margin': 50 # Margin around each floppy, does not overlap.
}

shadow = {
    'color': '0',
    'dx': '12',
    'dy': '-8',
    'std_deviation': '0',
    'opacity': '1'
}

filetypes = ['jpg']

