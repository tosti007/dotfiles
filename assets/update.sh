#!/usr/bin/bash

COLOR_NORMAL='\033[0m'
COLOR_BOLD='\033[1m'
COLOR_RED='\033[1;31m'
COLOR_GREEN='\033[1;32m'
COLOR_YELLOW='\033[1;33m'
SUMMARY_TEXT=''

function header() {
	echo -en "\n$COLOR_BOLD"
	echo -en "―― $1 $(seq $((${COLUMNS:-$(tput cols)} / 3 * 2 - ${#1} - 4)) | sed 'c ―' | tr -d '\n')"
	echo -e "$COLOR_NORMAL"
}

function ask-retry() {
	if [ "$?" -eq 0 ]; then
		return 0
	fi
	echo -en "\n$COLOR_YELLOW" >&2
	ask "Retry?" N >&2
	result="$?"
	echo -en "$COLOR_NORMAL" >&2
	return "$result"
}

function summary() {
	if [ "$?" -eq 0 ]; then
		SUMMARY_TEXT="$SUMMARY_TEXT$1:$COLOR_GREEN SUCCESS$COLOR_NORMAL\n"
	else
		SUMMARY_TEXT="$SUMMARY_TEXT$1:$COLOR_RED FAILURE$COLOR_NORMAL\n"
	fi
}

function summary-show() {
	header 'Summary'
	echo -e "$SUMMARY_TEXT"
}

function execute-loop() {
	eval "$1" || {
		if ! ask-retry; then
			return 1
		fi
		execute-loop "$1"
	}
}

function execute() {
	header "$1"
	execute-loop "$2"
	summary "$1"
}

