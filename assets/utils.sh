#!/usr/bin/env bash

## Echo to stderr
##
## $@: arguments to passed to echo
eecho() {
	>&2 echo $@
}

## Returns 0 when the passed command is available
##
## $1: command name
## $@: other args passed to 'command'
cmd_is_available() {
	command -v $@ &> /dev/null
}

## Check the return value of the previous call and exits with the same code if is
## was non-zero.
was_ok_or_exit() {
  local RET=$?
	if [ "$RET" -ne 0 ]; then
		exit "$RET"
	fi
}

## Loops over the second argument (default PATH) and prints the first hit and
## exits with 0. When none is found, print the input value and exit with 1.
##
## $1: name to search for
## $2: paths to search in, default: $PATH
find_first_exist_in_path() {
	if [ -f "$1" ]; then
		echo "$1"
		return 0
	fi
	( IFS=':'
		for PREFIX in ${2:-PATH}; do
			if [ -e "$PREFIX/$1" ]; then
				echo "$PREFIX/$1"
				exit 0
			fi
		done
		echo "$1"
		exit 1
	)
}

