#!/usr/bin/env bash

# Tools
cmd_is_available() {
	command -v $1 &> /dev/null
	if [ $? -ne 0 ]; then
		echo "Command '$1' is not available." >&2
		echo "Please make sure it's installed and available in the PATH." >&2
		exit 4
	fi
}

cmd_is_available 'pastel'

# General config
WALLPAPER_PATH="$HOME/.cache/background/main.png"

FONT_REGULAR="sans-serif"
FONT_BOLD="sans-serif:style=Bold"
FONT_ITALIC="sans-serif:style=Italic"
FONT_MONO="monospace"

BORDER_COLOR="#111a1a"
BORDER_SIZE=8
BORDER_RADIUS=0

# Colors
## Special
COLOR_ALPHA=90
COLOR_BACKGROUND="#1d2d2d"
COLOR_FOREGROUND="#eeead3"
COLOR_CURSOR="#ecd85a"

## Black
COLOR_BLACK="#111a1a"
COLOR_BLACK_LIGHT="#314c4c"
COLOR_0="$COLOR_BLACK"
COLOR_8="$COLOR_BLACK_LIGHT"

## Red
COLOR_RED="#ff5252"
COLOR_RED_LIGHT="#ff7d7d"
COLOR_1="$COLOR_RED"
COLOR_9="$COLOR_RED_LIGHT"

## Green
COLOR_GREEN="#53a255"
COLOR_GREEN_LIGHT="#7bbb7d"
COLOR_2="$COLOR_GREEN"
COLOR_10="$COLOR_GREEN_LIGHT"

## Yellow
COLOR_YELLOW="#ecd85a"
COLOR_YELLOW_LIGHT="#f0e183"
COLOR_3="$COLOR_YELLOW"
COLOR_11="$COLOR_YELLOW_LIGHT"

## Blue
COLOR_BLUE="#0f6999"
COLOR_BLUE_LIGHT="#2e86b8"
COLOR_4="$COLOR_BLUE"
COLOR_12="$COLOR_BLUE_LIGHT"

## Purple
COLOR_PURPLE="#886fb1"
COLOR_PURPLE_LIGHT="#b19ed2"
COLOR_5="$COLOR_PURPLE"
COLOR_13="$COLOR_PURPLE_LIGHT"

## Cyan
COLOR_CYAN="#4ab6b6"
COLOR_CYAN_LIGHT="#83dcdc"
COLOR_6="$COLOR_CYAN"
COLOR_14="$COLOR_CYAN_LIGHT"

## White
COLOR_WHITE="#989687"
COLOR_WHITE_LIGHT="#ccc9b5"
COLOR_7="$COLOR_WHITE"
COLOR_15="$COLOR_WHITE_LIGHT"

## Orange, (red + yellow) / 2
COLOR_ORANGE="$(pastel mix --colorspace=RGB $COLOR_RED $COLOR_YELLOW)"
COLOR_ORANGE_LIGHT="$(pastel mix --colorspace=RGB $COLOR_RED_LIGHT $COLOR_YELLOW_LIGHT)"

## Ocean, (cyan + green) / 2
COLOR_OCEAN="$(pastel mix --colorspace=RGB $COLOR_GREEN $COLOR_CYAN)"
COLOR_OCEAN_LIGHT="$(pastel mix --colorspace=RGB $COLOR_GREEN_LIGHT $COLOR_CYAN_LIGHT)"
