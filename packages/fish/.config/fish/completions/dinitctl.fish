#!/usr/bin/fish
# Fish completions for dinitctl

function __dinitctl_service_name
    command ls -F1 ~/.config/dinit.d/ | grep -v /
end

function __dinitctl_service_name_active
    dinitctl list 2>/dev/null | sed 's/^.*] //; s/ (pid:.*$//; s/ (exit status:.*$//'
end

# Disable file completions everywhere
complete -c dinitctl -f

# All options
complete -c dinitctl        -l 'help'        -d 'Display brief help text and then exit'
complete -c dinitctl        -l 'version'     -d 'Display version and then exit'
complete -c dinitctl -s 's' -l 'system'      -d 'Control the system init process (this is the default when run as root)'
complete -c dinitctl -s 'u' -l 'user'        -d 'Control the user init process (this is the default when not run as root)'
complete -c dinitctl -s 'p' -l 'socket-path' -d 'Specify the path to the socket used for communicating with the service manage…'
complete -c dinitctl        -l 'quiet'       -d 'Suppress status output, except for errors'

# All subcommands
complete -c dinitctl -n "__fish_is_first_token" -a 'start'     -d 'Start the specified service'
complete -c dinitctl -n "__fish_is_first_token" -a 'stop'      -d 'Stop the specified service, and remove explicit activation'
complete -c dinitctl -n "__fish_is_first_token" -a 'status'    -d 'Give a status report on the specified service'
complete -c dinitctl -n "__fish_is_first_token" -a 'restart'   -d 'Restart the specified service'
complete -c dinitctl -n "__fish_is_first_token" -a 'wake'      -d 'Start the specified service after reattaching dependency links'
complete -c dinitctl -n "__fish_is_first_token" -a 'release'   -d 'Clear the explicit activation mark from a service'
complete -c dinitctl -n "__fish_is_first_token" -a 'unpin'     -d 'Remove start- and stop- pins from a service'
complete -c dinitctl -n "__fish_is_first_token" -a 'unload'    -d 'Completely unload a service'
complete -c dinitctl -n "__fish_is_first_token" -a 'reload'    -d 'Attempt to reload a service description'
complete -c dinitctl -n "__fish_is_first_token" -a 'list'      -d 'List loaded services and their state'
complete -c dinitctl -n "__fish_is_first_token" -a 'shutdown'  -d 'Stop all services and terminate Dinit'
complete -c dinitctl -n "__fish_is_first_token" -a 'add-dep'   -d 'Add a dependency between two services'
complete -c dinitctl -n "__fish_is_first_token" -a 'rm-dep'    -d 'Remove a dependency between two services'
complete -c dinitctl -n "__fish_is_first_token" -a 'enable'    -d 'Persistently enable a waits-for dependency between two services'
complete -c dinitctl -n "__fish_is_first_token" -a 'disable'   -d 'Permanently disable a waits-for dependency between two services'
complete -c dinitctl -n "__fish_is_first_token" -a 'trigger'   -d 'Mark the specified service as having its external trigger set'
complete -c dinitctl -n "__fish_is_first_token" -a 'untrigger' -d 'Clear the trigger for the specified service'
complete -c dinitctl -n "__fish_is_first_token" -a 'setenv'    -d 'Export one or more variables into the activation environment'

# Add --no-wait option
complete -c dinitctl -n "__fish_seen_subcommand_from start"   -l 'no-wait' -d 'Do not wait for issued command to complete; exit immediately'
complete -c dinitctl -n "__fish_seen_subcommand_from stop"    -l 'no-wait' -d 'Do not wait for issued command to complete; exit immediately'
complete -c dinitctl -n "__fish_seen_subcommand_from restart" -l 'no-wait' -d 'Do not wait for issued command to complete; exit immediately'
complete -c dinitctl -n "__fish_seen_subcommand_from wake"    -l 'no-wait' -d 'Do not wait for issued command to complete; exit immediately'

# Add --pin option
complete -c dinitctl -n "__fish_seen_subcommand_from start"   -l 'pin' -d 'Pin the service in the requested state'
complete -c dinitctl -n "__fish_seen_subcommand_from stop"    -l 'pin' -d 'Pin the service in the requested state'

# Add --ignore-unstarted option
complete -c dinitctl -n "__fish_seen_subcommand_from stop"    -l 'ignore-unstarted' -d "If the service is not started or doesn\'t exist, ignore the command and return"
complete -c dinitctl -n "__fish_seen_subcommand_from restart" -l 'ignore-unstarted' -d "If the service is not started or doesn\'t exist, ignore the command and return"
complete -c dinitctl -n "__fish_seen_subcommand_from release" -l 'ignore-unstarted' -d "If the service is not started or doesn\'t exist, ignore the command and return"

# Add --ignore-unstarted option
complete -c dinitctl -n "__fish_seen_subcommand_from status"  -l 'ignore-unstarted' -d "If the service is not started or doesn\'t exist, ignore the command and return"
complete -c dinitctl -n "__fish_seen_subcommand_from unpin"   -l 'ignore-unstarted' -d "If the service is not started or doesn\'t exist, ignore the command and return"
complete -c dinitctl -n "__fish_seen_subcommand_from unload"  -l 'ignore-unstarted' -d "If the service is not started or doesn\'t exist, ignore the command and return"
complete -c dinitctl -n "__fish_seen_subcommand_from reload"  -l 'ignore-unstarted' -d "If the service is not started or doesn\'t exist, ignore the command and return"

# Add --force option
complete -c dinitctl -n "__fish_seen_subcommand_from stop"    -l 'force' -d 'Stop the service even if it will require stopping other services which depend'
complete -c dinitctl -n "__fish_seen_subcommand_from restart" -l 'force' -d 'Stop the service even if it will require stopping other services which depend'

# Add service-name argument
complete -c dinitctl -n '__fish_seen_subcommand_from start   && __fish_is_nth_token 2' -a '(__dinitctl_service_name)'
complete -c dinitctl -n '__fish_seen_subcommand_from stop    && __fish_is_nth_token 2' -a '(__dinitctl_service_name_active)'
complete -c dinitctl -n '__fish_seen_subcommand_from status  && __fish_is_nth_token 2' -a '(__dinitctl_service_name_active)'
complete -c dinitctl -n '__fish_seen_subcommand_from restart && __fish_is_nth_token 2' -a '(__dinitctl_service_name_active)'
complete -c dinitctl -n '__fish_seen_subcommand_from wake    && __fish_is_nth_token 2' -a '(__dinitctl_service_name)'
complete -c dinitctl -n '__fish_seen_subcommand_from release && __fish_is_nth_token 2' -a '(__dinitctl_service_name_active)'
complete -c dinitctl -n '__fish_seen_subcommand_from unpin   && __fish_is_nth_token 2' -a '(__dinitctl_service_name_active)'
complete -c dinitctl -n '__fish_seen_subcommand_from unload  && __fish_is_nth_token 2' -a '(__dinitctl_service_name_active)'
complete -c dinitctl -n '__fish_seen_subcommand_from reload  && __fish_is_nth_token 2' -a '(__dinitctl_service_name_active)'
complete -c dinitctl -n '__fish_seen_subcommand_from enable  && __fish_is_nth_token 2' -a '(__dinitctl_service_name)'
complete -c dinitctl -n '__fish_seen_subcommand_from disable && __fish_is_nth_token 2' -a '(__dinitctl_service_name)'

# Subcommand add-dep
complete -c dinitctl -n '__fish_seen_subcommand_from add-dep && __fish_is_nth_token 2' -a 'regular milestone waits-for'
complete -c dinitctl -n '__fish_seen_subcommand_from add-dep && __fish_is_nth_token 3' -a '(__dinitctl_service_name)'
complete -c dinitctl -n '__fish_seen_subcommand_from add-dep && __fish_is_nth_token 4' -a '(__dinitctl_service_name)'

# Subcommand rm-dep
complete -c dinitctl -n '__fish_seen_subcommand_from rm-dep && __fish_is_nth_token 2' -a 'regular milestone waits-for'
complete -c dinitctl -n '__fish_seen_subcommand_from rm-dep && __fish_is_nth_token 3' -a '(__dinitctl_service_name)'
complete -c dinitctl -n '__fish_seen_subcommand_from rm-dep && __fish_is_nth_token 4' -a '(__dinitctl_service_name)'

# Subcommand enable
complete -c dinitctl -n "__fish_seen_subcommand_from enable && not __fish_seen_argument -l from" -l 'from'
complete -c dinitctl -n "__fish_seen_subcommand_from enable && __fish_seen_argument -l from && __fish_is_nth_token 3" -a '(__dinitctl_service_name)'

# Subcommand disable
complete -c dinitctl -n "__fish_seen_subcommand_from disable && not __fish_seen_argument -l from" -l 'from'
complete -c dinitctl -n "__fish_seen_subcommand_from disable && __fish_seen_argument -l from && __fish_is_nth_token 3" -a '(__dinitctl_service_name)'

# Subcommand setenv
complete -c dinitctl -n "__fish_seen_subcommand_from setenv" -F
