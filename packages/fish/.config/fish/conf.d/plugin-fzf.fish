# Configure fish-fzf settings

if not type -q fzf; or not status is-interactive
    return
end

if not type -q fzf_configure_bindings
    return
end

set -U fzf_history_opts '--preview-window=hidden'
fzf_configure_bindings \
    --directory=\cf \
    --git_log=\cg \
    --git_status \
    --history=\cr \
    --variables=\cv \
    --processes=\cu

