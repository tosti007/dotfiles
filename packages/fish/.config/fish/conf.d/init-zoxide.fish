# Set zoxide's environment in interactive mode only
if not type -q zoxide; or not status is-interactive
    return
end

zoxide init fish --hook pwd --cmd cd | source

