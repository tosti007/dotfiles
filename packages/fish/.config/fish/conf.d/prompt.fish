# Mostly obtained from https://github.com/oh-my-fish/theme-cmorrell.com

# Remove the default greeting
set -U fish_greeting ""

# Remove prepending the prompt with (env) when an virtual environment is active
set -U VIRTUAL_ENV_DISABLE_PROMPT true

function prompt_segment -d "Function to show a segment"
  # Set colors
  set_color -b $argv[1]
  set_color $argv[2]

  # Print text
  if [ -n "$argv[3]" ]
    echo -n $argv[3]
  end
end

function show_git_status -d "Gets the current git status"
  if command git rev-parse --is-inside-work-tree >/dev/null 2>&1
    set -l git_dir (command git rev-parse --git-dir)
    set -l dirty (command git status -s --ignore-submodules=dirty | wc -l | sed -e 's/^ *//' -e 's/ *$//' 2> /dev/null)
    set -l ref

    # Taken from fish_git_prompt.sh
    if test -d $git_dir/rebase-merge
      set ref "REBASE"
      set step (cat $git_dir/rebase-merge/msgnum 2>/dev/null)
      set total (cat $git_dir/rebase-merge/end 2>/dev/null)
      if test -n "$step" -a -n "$total"
        set ref "$ref $step/$total"
      end
    else if test -d $git_dir/rebase-apply
      set ref "REBASE"
      set step (cat $git_dir/rebase-apply/next 2>/dev/null)
      set total (cat $git_dir/rebase-apply/last 2>/dev/null)
      if test -n "$step" -a -n "$total"
        set ref "$ref $step/$total"
      end
    else if test -f $git_dir/MERGE_HEAD
      set ref "MERGE"
    else if test -f $git_dir/CHERRY_PICK_HEAD
      set ref "CHERRY-PICK"
    else if test -f $git_dir/REVERT_HEAD
      set ref "REVERT"
    else if test -f $git_dir/BISECT_LOG
      set ref "BISECT"
    else if set ref (command git describe --tags --exact-match 2>/dev/null)
    else if set ref (command git symbolic-ref --short HEAD 2>/dev/null)
    else
      set ref (command git rev-parse --short HEAD 2>/dev/null)
    end

    if [ "$dirty" != "0" ]
      prompt_segment red black " $ref "
    else
      prompt_segment blue black " $ref "
    end
    set_color normal
    echo -n " "
   end
end

function show_virtualenv -d "Show active python virtual environments"
  if set -q VIRTUAL_ENV
    set -l venvname (basename "$VIRTUAL_ENV")
    prompt_segment normal white "($venvname) "
  end
end

function show_character -d "Shows prompt character with cue for current priv"
  set -l uid (id -u $USER)
  if [ $uid -eq 0 ]
    prompt_segment red white " ! "
  else
    if [ $RETVAL -ne 0 ]
      prompt_segment red black "%"
    else
      prompt_segment normal yellow "%"
    end
  end

  set_color normal
  echo -n " "
end

function fish_prompt -d "Prints left prompt"
  set -g RETVAL $status
  show_git_status
  show_virtualenv
  show_character
  # Set the terminal cursor to a line
  echo -e -n "\033[5 q"
end

function show_pwd -d "Show the current directory"
  set -l pwd (prompt_pwd)
  prompt_segment normal $fish_color_cwd " $pwd "
end

function fish_right_prompt -d "Prints right prompt"
  show_pwd
  set_color normal
end

