# Build in overwrites
alias cat bat
alias diff delta
alias extract "file-roller --extract-here --notify"
alias rg "rg -S"
alias h hx
alias ctan tllocalmgr
alias foot footclient
alias krop "krop --output /tmp/Downloads/krop.pdf --selections individual"
alias lsblk "lsblk --output NAME,TYPE,FSTYPE,SIZE,PARTTYPENAME,MOUNTPOINTS -A"

# ls shortcuts, but with exa because that's cooler
alias ls lsd
alias ll "ls -l"
alias la "ll -a"
alias tree "ls --tree"

# Command shortcuts
alias g git
alias rmr "rm -r"
alias py ipython

alias copy wl-copy
alias paste wl-paste

## Swayhide shortcuts
alias hide swayhide
alias hideopen "hide xdg-open"

alias xwayland "env -u WAYLAND_DISPLAY env GDK_BACKEND='x11'"
