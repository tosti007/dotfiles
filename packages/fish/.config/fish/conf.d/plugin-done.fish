# Configure fish-done settings
set -U __done_notification_command 'notify-send $title $message'
set -U __done_notification_urgency_level low
set -U __done_notification_urgency_level_failure normal
set -U __done_sway_ignore_visible 1
set -e __done_notify_sound

