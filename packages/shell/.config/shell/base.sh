#!/bin/sh

DIR="$HOME/.config/shell"

if test -f "$DIR/xdg.sh"; then
	source "$DIR/xdg.sh"
fi

if test -f "$DIR/wayland.sh"; then
	source "$DIR/wayland.sh"
fi

if test -f "$DIR/zoxide.sh"; then
	source "$DIR/zoxide.sh"
fi

if test -f "$HOME/.config/nnn/config"; then
    source "$HOME/.config/nnn/config"
fi

# Personal preferences
export EDITOR="hx"
export VISUAL="$EDITOR"
export GUI_EDITOR="$EDITOR"
export BROWSER="firefox"
export TERMINAL="footclient"
export PAGER="less -RF"

# Update path
if test -d "/opt/homebrew/bin"; then
	export PATH="/opt/homebrew/bin:$PATH"
fi

export PATH="$HOME/.local/bin:$PATH"
export PATH="$CARGO_HOME/bin:$PATH"
export PATH="$HOME/.dotfiles/bin:$PATH"

