#!/bin/sh

# exa does not accept the default ls options of zoxide
if command -v zoxide >/dev/null; then
	# reset
	export _ZO_FZF_OPTS=''
	# default sane settings
	export _ZO_FZF_OPTS="$_ZO_FZF_OPTS --no-sort"
	export _ZO_FZF_OPTS="$_ZO_FZF_OPTS --keep-right"
	export _ZO_FZF_OPTS="$_ZO_FZF_OPTS --exit-0"
	export _ZO_FZF_OPTS="$_ZO_FZF_OPTS --select-1"
	export _ZO_FZF_OPTS="$_ZO_FZF_OPTS --bind=ctrl-z:ignore"
	# layout
	export _ZO_FZF_OPTS="$_ZO_FZF_OPTS --height=100%"
	export _ZO_FZF_OPTS="$_ZO_FZF_OPTS --layout=reverse"
	export _ZO_FZF_OPTS="$_ZO_FZF_OPTS --info=inline"
	# preview command
	export _ZO_FZF_OPTS="$_ZO_FZF_OPTS --preview-window=down"
	export _ZO_FZF_OPTS="$_ZO_FZF_OPTS --preview='ls {2..}'"
fi

