#!/bin/sh

# Set XDG values to defaults
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CACHE_HOME="$HOME/.cache"

# Moving data into XDG dirs, obtained through xdg-ninja
export ANDROID_HOME="$XDG_DATA_HOME/android"
export HISTFILE="$XDG_STATE_HOME/bash/history"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
export IPYTHONDIR="$XDG_CONFIG_HOME/ipython"
export LESSHISTFILE="$XDG_CACHE_HOME/less/history"
export MPLAYER_HOME="$XDG_CONFIG_HOME/mplayer"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"

