#!/bin/bash

# Quit the script if any command fails
set -e

cd "$(dirname "$0")"

function powermenu-write() {
	shutdown='Shutdown\0icon\x1fkshutdown'
	lock='Lock\0icon\x1fsystem-lock-screen'
	reboot='Reboot\0icon\x1fsystem-restart'
	hibernate='Exit WM\0icon\x1fsystem-hibernate'
	echo -e "$shutdown"
	echo -e "$lock"
	echo -e "$reboot"
	echo -e "$hibernate"
}

function powermenu-read() {
	read line
	case "$line" in
	Lock)
		spotify lock
		loginctl lock-session
		;;
	Hibernate)
		systemctl hibernate
		;;
	Reboot)
		systemctl reboot
		;;
	Shutdown)
		systemctl poweroff
		;;
	'Exit WM')
		close-wm
		;;
	esac
}

case $1 in
fullscreen)
	rofi -theme fullscreen -show drun
	;;
windows)
	rofi -theme windows -show window -selected-row 1
	;;
powermenu)
	powermenu-write | rofi -theme powermenu -dmenu | powermenu-read
	;;
esac

