#!/usr/bin/bash

set -e

cd ~/.cache/background

# For a round rectangle:
#roundrectangle 835,415 1085,665 6,6

magick main.png \
	-blur 6x6 \
	-draw "fill #000000bf circle 960,540 1050,630" \
	swaylock.png

