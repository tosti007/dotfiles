#!/usr/bin/bash

if [ -f /tmp/playerctl_lock_resume ]; then
	playerctl play
	rm /tmp/playerctl_lock_resume
fi

