#!/usr/bin/bash

if pgrep swaylock; then
	exit
fi

if [ "$1" != 'idle' ] && [ "$(playerctl status 2> /dev/null)" == 'Playing' ]; then
	touch /tmp/playerctl_lock_resume
	playerctl pause
fi

rbw lock

(swaylock --config ~/.cache/ruwal/swaylock.conf $@ && loginctl unlock-session) &

