# Dotfiles

These dotfiles are managed using
[Git](https://git-scm.com/) and
[GNU Stow](https://www.gnu.org/software/stow/stow.html).

## Quick installation

Clone and install:

```shell
$ git clone https://codeberg.org/tosti007/dotfiles.git ~/.dotfiles
$ cd ~/.dotfiles
$ ./install.sh
```

## Directory structure
 - **install.sh** links the configuration files to their correct location. If no arguments are given it will iterate over all folders in `user` and `system`. To install only a single configuration group the path relative to the installation script (e.g. `user/sway`) can be passed. There are no other options or commands.
 - **assets** additional files, such as common bash utilities and wallpapers.
 - **bin** contains some command line scripts.
 - **user** holds folders containing configuration files that should be placed in `$HOME`.
 - **system** holds folders containing configuration files that should be placed in `/`.

### config folders
The `user` and `system` folders hold sub-folder with the actual configuration files. The names of these sub-folders are arbitrary and I grouped the config per-package basis.

### install programs
When a config folder contains an executable programm called 'install', it will be executed instead of stowing the folder.

## Attributions
Here is a list of attributions that I could not add within other files, but still wanted to list.

**Original dotfiles list**
I originally worked this repo from [Robbert van der Helm](https://github.com/robbert-vdh/dotfiles). Many thanks to him for showing me the ropes of linux and git. Our repos have since then quite diverged, so it might be interesting to see what he's up to these days.

**assets/timezone-coordinates.csv**
Originally joinked from [jjfalling/iana-timezone-to-coordinates](https://github.com/jjfalling/iana-timezone-to-coordinates), a repo which has now been archived. To ensure my scripts keep working, and with some minor changes, I opted to copy the file instead.
